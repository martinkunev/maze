struct track;

struct context_audio
{
	void *stream;
	const char *playing;
	const struct track *track;
	size_t offset;
};

int audio_init(struct context_audio *context);
int audio_play(struct context_audio *restrict context, const char *restrict filename);
int audio_stop(struct context_audio *context);
int audio_term(struct context_audio *context);
