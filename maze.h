#define EQUIP(n) (1 << (n))

#define WIDTH_LIMIT 12
#define OBJECTS_LIMIT 16
#define ELEVATORS_LIMIT 3
#define FLOORS_LIMIT 6

enum {NEIGHBOR_TOP_LEFT, NEIGHBOR_LEFT, NEIGHBOR_BOTTOM_LEFT, NEIGHBOR_TOP_RIGHT, NEIGHBOR_RIGHT, NEIGHBOR_BOTTOM_RIGHT, /* last */ NEIGHBORS_LIMIT};

struct maze
{
	struct player
	{
		unsigned room;
		struct {unsigned x, y;} location;
		unsigned items;
		int elevator;
	} player;

	struct room
	{
		const char *name;
		const char *music;
		unsigned char music_item; // item disabling the music
		unsigned neighbors[NEIGHBORS_LIMIT];
		unsigned count;
		struct object
		{
			enum {OBJECT_NONE = 0, OBJECT_DOOR, OBJECT_OBSTACLE, OBJECT_ITEM, OBJECT_SWITCH, OBJECT_HINT} type;
			union link
			{
				struct
				{
					unsigned number;
					enum {DOOR_LOW, DOOR_HIGH, DOOR_CEILING, DOOR_ELEVATOR, DOOR_HOLE} type;
				} room; // door
				unsigned position; // obstacle, item, switch
				struct {const char *decoration; const char *message;} hint; // hint
			} info;
		} layout[OBJECTS_LIMIT];
		unsigned color;
		unsigned twin;
	} rooms[256];

	unsigned char elevator[ELEVATORS_LIMIT][FLOORS_LIMIT]; // WARNING: elevator 0 is reserved (indicating "not in elevator")

	unsigned room_victory;
};

struct maze *maze0(int cheats);
struct maze *maze_updated(int cheats);

unsigned colorindex(unsigned char foreground, unsigned char background);
