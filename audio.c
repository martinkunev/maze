#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <math.h>

#include <portaudio.h>
#include <vorbis/vorbisfile.h>

#include "audio.h"

#if !defined(BYTE_ORDER)
# error "Compiler flag missing. Cannot detect endianness"
#endif
#if (BYTE_ORDER == BIG_ENDIAN)
# define BIGENDIAN 1
#elif (BYTE_ORDER == LITTLE_ENDIAN)
# define BIGENDIAN 0
#else
# error "Unable to detect endianness."
#endif

#define BUFFER_SIZE 4096
#define OGG_FRAMES 64

struct track
{
	size_t count;
	double length;
	unsigned rate;
	unsigned char channels;
	int16_t data[];
};

static struct track *track_alloc(const char *filename)
{
	struct track *track;

	FILE *ogg;
	struct OggVorbis_File file;
	struct vorbis_info *track_info;
	double track_length;
	long size;

	int bitstream = 0; // TODO do I need to use this?

	ogg = fopen(filename, "rb");
	if (ov_open(ogg, &file, NULL, 0) < 0)
	{
		fclose(ogg);
		return 0;
	}

	track_length = ov_time_total(&file, -1);
	track_info = ov_info(&file, -1);

	track = malloc(sizeof(*track) + ceil(track_length * track_info->rate * track_info->channels * sizeof(*track->data) + BUFFER_SIZE)); // TODO is the calculation for count correct?
	if (!track)
		goto finally;
	track->length = track_length;
	track->rate = track_info->rate;
	track->channels = track_info->channels;
	track->count = 0;
 
	//printf("%f seconds\nchannels: %i\nsample rate: %li\n", track_length, track_info->channels, track_info->rate);

	ov_pcm_seek(&file, 0);

	while (size = ov_read(&file, (char *)(track->data + track->count), BUFFER_SIZE, BIGENDIAN, sizeof(*track->data), 1, &bitstream))
	{
		if (size < 0)
		{
			free(track);
			track = 0;
			goto finally;
		}
		assert(sizeof(*track->data) % 2 == 0);
		track->count += size / sizeof(*track->data);
	}

finally:
	ov_clear(&file);
	return track;
}

static int audio_next(const void *input, void *output, unsigned long frames, const PaStreamCallbackTimeInfo *time_info, PaStreamCallbackFlags flags, void *argument)
{
	struct context_audio *restrict context = argument;
	size_t count = OGG_FRAMES * context->track->channels;
	size_t count_left = context->track->count - context->offset;

	if (count_left < count)
	{
		memcpy(output, context->track->data + context->offset, count_left * sizeof(*context->track->data));
		context->offset = 0;
		count -= count_left;
		output = (char *)output + count_left * sizeof(*context->track->data);
	}
	if (count)
	{
		memcpy(output, context->track->data + context->offset, count * sizeof(*context->track->data));
		context->offset += count;
	}

	// return paAbort;
	// return paComplete;
	return paContinue;
}

int audio_init(struct context_audio *context)
{
	PaError status;

	context->track = 0;

	status = Pa_Initialize();
	if (status != paNoError)
		return -1;

	/*status = Pa_SetStreamFinishedCallback(stream, &audio_end);
	if (status != paNoError)
		goto error;*/

	return 0;
}

int audio_play(struct context_audio *restrict context, const char *restrict filename)
{
	PaError status;
	PaStreamParameters parameters;

	if (context->track && !strcmp(filename, context->playing))
		return 0; // already playing the same track; nothing to do

	status = audio_stop(context);
	if (status)
		return -2;

	context->track = track_alloc(filename);
	if (!context->track)
		return -1;
	context->offset = 0;

	parameters.device = Pa_GetDefaultOutputDevice();
	if (parameters.device == paNoDevice)
		return -3;
	parameters.suggestedLatency = Pa_GetDeviceInfo(parameters.device)->defaultLowOutputLatency;
	parameters.channelCount = context->track->channels;
	parameters.sampleFormat = paInt16;
	parameters.hostApiSpecificStreamInfo = 0;

	status = Pa_OpenStream(&context->stream, 0, &parameters, context->track->rate, OGG_FRAMES, paClipOff, &audio_next, context);
	if (status != paNoError)
		return -4;

	sleep(1); // TODO there is lag in the beginning without this; see why

	status = Pa_StartStream(context->stream);
	if (status != paNoError)
		return -5;

	context->playing = filename;
	return 0;
}

int audio_stop(struct context_audio *context)
{
	if (context->track)
	{
		PaError status;

		assert(context->stream);

		status = Pa_AbortStream(context->stream);
		//status = Pa_StopStream(context->stream);
		if (status != paNoError)
			return status;

		status = Pa_CloseStream(context->stream);
		if (status != paNoError)
			return status;

		free((struct track *)context->track);
		context->track = 0;
	}

	return 0;
}

int audio_term(struct context_audio *context)
{
	PaError status = audio_stop(context);
	Pa_Terminate();
	return status;
}

/*
int main(int argc, char *argv[])
{
	struct context_audio context;

	if (argc != 2)
		return 1;

	if (audio_init(&context) < 0)
		abort();

	if (audio_play(&context, argv[1]) < 0)
		abort();

	sleep(60);

	audio_term(&context);

	return 0;
}
*/

#if 0

/*static void audio_end(void *argument)
{
	struct context_audio *restrict context = argument;
}*/

// fprintf(stderr, "error: %s (%i)\n", Pa_GetErrorText(status), status);

#endif
