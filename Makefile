export CC=gcc
#export CFLAGS=-std=c99 -pedantic -Werror=vla -D_POSIX_C_SOURCE=200809L -DNDEBUG -pthread -O2 -fstrict-aliasing -fomit-frame-pointer
export CFLAGS=-std=c99 -pedantic -Werror=vla -D_DEFAULT_SOURCE -DNDEBUG -DSOUND_ON -pthread -O2 -fstrict-aliasing -fomit-frame-pointer
#export CFLAGS=-std=c99 -pedantic -Werror=vla -D_DEFAULT_SOURCE -pthread -O0 -g
export LDFLAGS=-pthread
export LDLIBS=-lncurses /usr/lib/x86_64-linux-gnu/libportaudio.a -lm -lasound -ljack -lvorbisfile

all: maze

audio: audio.o

maze: maze.o maps.o audio.o

clean:
	rm -f *.o
	rm -f maze
