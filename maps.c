#include <assert.h>
#include <stdlib.h>

#include <ncurses.h>

#include "maze.h"

#define NAME_HALL "Hall"
#define NAME_LIBRARY "Library"
#define NAME_OFFICE "Office"
#define NAME_GYM "Gym"
#define NAME_CLASSROOM "Classroom"
#define NAME_CAFETERIA "Cafeteria"
#define NAME_KITCHEN "Kitchen"
#define NAME_LOCKERS "Lockers"
#define NAME_WEIGHT_ROOM "Weight Room"
#define NAME_NURSE "Nurse"
#define NAME_MUSIC_ROOM "Music Room"
#define NAME_CHEMISTRY "Chemistry"
#define NAME_ROOFTOP "Rooftop"
#define NAME_ANATOMY "Anatomy"
#define NAME_AUTO_SHOP "Auto Shop"
#define NAME_CRAWL_SPACE "Crawl Space"
#define NAME_AIR_DUCTS "Air Ducts"
#define NAME_BASEMENT "Basement"
#define NAME_BOILER_ROOM "Boiler Room" /* final boss room */
#define NAME_ENTRANCE "Entrance"
#define NAME_WARP_ZONE "Warp Zone"
#define NAME_STORAGE_ROOM "Storage Room" /* new */

#define SIGN "x\n\n\n\n"

#define NEIGHBORS(...) {__VA_ARGS__}
#define DOOR(...) {OBJECT_DOOR, .info.room = {__VA_ARGS__ + DOOR_LOW}}
#define HOLE(n) {OBJECT_DOOR, .info.room = {n, DOOR_HOLE}}
#define OBSTACLE(n) {OBJECT_OBSTACLE, (n)}
#define ITEM(n) {OBJECT_ITEM, (n)}
#define ELEVATOR(n) {OBJECT_DOOR, .info.room = {(n), DOOR_ELEVATOR}}
#define EMPTY {OBJECT_NONE}
#define HINT(s) {OBJECT_HINT, .info.hint = {SIGN, (s)}}
#define OBJECT(d, s) {OBJECT_HINT, .info.hint = {(d), (s)}}

#define SWITCH_CLOCK {OBJECT_SWITCH, 22}
#define SWITCH_ELEVATOR {OBJECT_SWITCH, 23}
#define SWITCH_WATER {OBJECT_SWITCH, 24}

#define LAYOUT(...) .layout = {__VA_ARGS__}, .count = (sizeof((struct object []){__VA_ARGS__}) / sizeof(struct object))

struct maze *maze0(int cheats)
{
	static struct maze maze =
	{
		.rooms[1] = {
			.name = NAME_ENTRANCE,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 39, 0, 3, 4, 5),
			LAYOUT(EMPTY, OBSTACLE(9), EMPTY, DOOR(51), EMPTY, DOOR(52), EMPTY, DOOR(53), EMPTY)
		},
		.rooms[2] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 7),
			LAYOUT(EMPTY, DOOR(54), EMPTY)
		},
		.rooms[3] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 1, 6, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[4] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 1, 0, 7, 0, 0),
			LAYOUT(EMPTY, DOOR(55), EMPTY)
		},
		.rooms[5] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(1, 0, 0, 8, 0, 10),
			LAYOUT(EMPTY, DOOR(56), EMPTY, DOOR(57), EMPTY)
		},
		.rooms[6] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 3, 0, 11, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[7] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(2, 0, 4, 11, 12, 13),
			LAYOUT(EMPTY, DOOR(58), EMPTY, DOOR(59), EMPTY)
		},
		.rooms[8] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 5, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(60), EMPTY)
		},
		.rooms[9] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 13, 14, 0),
			LAYOUT(EMPTY, DOOR(77), EMPTY)
		},
		.rooms[10] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(5, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[11] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 6, 7, 0, 0, 0),
			LAYOUT(EMPTY, ELEVATOR(1), EMPTY)
		},
		.rooms[12] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 7, 0, 16, 0, 18),
			LAYOUT(EMPTY, OBSTACLE(2), EMPTY, DOOR(62), EMPTY)
		},
		.rooms[13] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(7, 0, 9, 29, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[14] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 9, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(92), EMPTY)
		},
		.rooms[15] = {
			.name = NAME_WARP_ZONE,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(34, 28, 29, 11, 14, 21),
			LAYOUT(EMPTY, DOOR(39), EMPTY)
		},

		.rooms[16] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 12, 0, 0, 19),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[17] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 19, 21),
			LAYOUT(EMPTY, DOOR(63), EMPTY, DOOR(64), EMPTY, DOOR(65), EMPTY)
		},
		.rooms[18] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(12, 0, 0, 20, 0, 22),
			LAYOUT(EMPTY, DOOR(66), EMPTY)
		},
		.rooms[19] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(16, 17, 0, 23, 24, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[20] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 18, 0, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[21] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(17, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ELEVATOR(1), EMPTY, DOOR(103), EMPTY)
		},
		.rooms[22] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(18, 22, 0, 0, 22, 0),
			LAYOUT(EMPTY, DOOR(68), EMPTY)
		},
		.rooms[23] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 19, 0, 25, 26),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[24] = {
			.name = NAME_HALL,
			.music = "spooky.ogg",
			.music_item = 22,
			.neighbors = NEIGHBORS(0, 19, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(69), EMPTY, OBSTACLE(22), EMPTY, DOOR(70), EMPTY, DOOR(71), EMPTY, DOOR(101), EMPTY)
		},
		.rooms[25] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 23, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(72), EMPTY)
		},
		.rooms[26] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(23, 0, 27, 0, 0, 0),
			LAYOUT(EMPTY, ELEVATOR(1), EMPTY)
		},
		.rooms[27] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 28, 0, 26, 0, 0),
			LAYOUT(EMPTY, DOOR(73), EMPTY, DOOR(74), EMPTY, DOOR(75), EMPTY)
		},
		.rooms[28] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 27, 0),
			LAYOUT(EMPTY, DOOR(100), EMPTY)
		},
		.rooms[29] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 13, 30, 0, 31),
			LAYOUT(EMPTY, OBSTACLE(12), EMPTY)
		},
		.rooms[30] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 29, 0, 37, 0),
			LAYOUT(EMPTY, DOOR(96), EMPTY)
		},
		.rooms[31] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(29, 0, 0, 0, 32, 33),
			LAYOUT(EMPTY, DOOR(76), EMPTY)
		},
		.rooms[32] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 31, 0, 0, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[33] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(31, 0, 34, 0, 0, 0),
			LAYOUT(EMPTY, OBSTACLE(13), EMPTY, OBSTACLE(8), EMPTY, OBSTACLE(4), SWITCH_ELEVATOR, EMPTY, ELEVATOR(1), EMPTY, DOOR(78), EMPTY)
		},
		.rooms[34] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 33, 0, 35),
			LAYOUT(EMPTY, DOOR(79), EMPTY, DOOR(80), EMPTY, DOOR(81), EMPTY)
		},
		.rooms[35] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(34, 0, 36, 0, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[36] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 35, 0, 0),
			LAYOUT(EMPTY, DOOR(107), EMPTY)
		},
		.rooms[37] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 30, 0, 0, 38, 0),
			LAYOUT(EMPTY, OBSTACLE(13), EMPTY)
		},
		.rooms[38] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 37, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(97), EMPTY)
		},

		.rooms[39] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(40, 0, 41, 0, 1, 0),
			LAYOUT(EMPTY, DOOR(84, DOOR_CEILING), EMPTY, OBSTACLE(13), EMPTY, DOOR(106), EMPTY)
		},
		.rooms[40] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 39),
			LAYOUT(EMPTY, DOOR(82), EMPTY)
		},
		.rooms[41] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 39, 0, 0),
			LAYOUT(EMPTY, DOOR(83), EMPTY)
		},
		.rooms[42] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 84, 0, 0, 0, 0),
			LAYOUT(EMPTY, OBSTACLE(12), EMPTY, DOOR(43), EMPTY)
		},

		.rooms[43] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 143,
			LAYOUT(EMPTY, EMPTY, DOOR(45), EMPTY, EMPTY, DOOR(42), EMPTY, DOOR(46), EMPTY)
		},
		.rooms[44] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 144,
			LAYOUT(EMPTY, DOOR(144), EMPTY, DOOR(46), EMPTY, DOOR(144), EMPTY)
		},
		.rooms[45] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 145,
			LAYOUT(EMPTY, EMPTY, DOOR(144), EMPTY, DOOR(43), EMPTY, DOOR(146), EMPTY)
		},
		.rooms[46] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 146,
			LAYOUT(EMPTY, DOOR(145), EMPTY, DOOR(43), EMPTY, DOOR(44), EMPTY, EMPTY)
		},

		.rooms[143] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 43,
			LAYOUT(EMPTY, DOOR(143), EMPTY, DOOR(143), EMPTY, ITEM(9), EMPTY, DOOR(145), EMPTY)
		},
		.rooms[144] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 44,
			LAYOUT(EMPTY, DOOR(44), EMPTY, DOOR(45), EMPTY, DOOR(44), EMPTY)
		},
		.rooms[145] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 45,
			LAYOUT(EMPTY, DOOR(46), EMPTY, HINT("Danger!"), DOOR(0), EMPTY, DOOR(143), EMPTY)
		},
		.rooms[146] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 46,
			LAYOUT(EMPTY, DOOR(108), EMPTY, DOOR(0), HINT("Danger!"), EMPTY, DOOR(45), EMPTY)
		},

		.rooms[51] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(1), EMPTY)
		},
		.rooms[52] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(1), EMPTY)
		},
		.rooms[53] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(1), EMPTY)
		},
		.rooms[54] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(2), EMPTY)
		},
		.rooms[55] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(4), EMPTY)
		},
		.rooms[56] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(5), EMPTY)
		},
		.rooms[57] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(5), EMPTY)
		},
		.rooms[58] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(7), EMPTY)
		},
		.rooms[59] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(7), EMPTY)
		},
		.rooms[60] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(8), EMPTY)
		},

		.rooms[62] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(12), EMPTY)
		},
		.rooms[63] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(17), EMPTY)
		},
		.rooms[64] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(17), EMPTY)
		},
		.rooms[65] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(17), EMPTY)
		},
		.rooms[66] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(18), EMPTY)
		},

		.rooms[68] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(22), EMPTY)
		},
		.rooms[69] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY)
		},
		.rooms[70] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY)
		},
		.rooms[71] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY)
		},
		.rooms[72] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(25), EMPTY)
		},
		.rooms[73] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(27), EMPTY)
		},
		.rooms[74] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(27), EMPTY)
		},
		.rooms[75] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(27), EMPTY)
		},
		.rooms[76] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(31), EMPTY)
		},
		.rooms[77] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(9), EMPTY)
		},
		.rooms[78] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(33), EMPTY)
		},
		.rooms[79] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(34), EMPTY)
		},
		.rooms[80] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(34), EMPTY)
		},
		.rooms[81] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(34), EMPTY)
		},
		.rooms[82] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(40), EMPTY)
		},
		.rooms[83] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(41), EMPTY)
		},

		.rooms[84] = {
			.name = NAME_CRAWL_SPACE,
			.music = "crawl.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 42, 0),
			LAYOUT(EMPTY, HOLE(39), EMPTY, OBSTACLE(12), EMPTY)
		},

		.rooms[91] = {
			.name = NAME_ROOFTOP,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(106), EMPTY, OBSTACLE(13), EMPTY, OBSTACLE(12), EMPTY, ELEVATOR(1), EMPTY)
		},
		.rooms[92] = {
			.name = NAME_LOCKERS,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(14), EMPTY, ITEM(2), EMPTY, DOOR(93), EMPTY)
		}, // showers
		.rooms[93] = {
			.name = NAME_GYM,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(HOLE(95), DOOR(92), EMPTY, DOOR(94), EMPTY)
		}, // scoreboard
		.rooms[94] = {
			.name = NAME_WEIGHT_ROOM,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(93), EMPTY, ITEM(12), EMPTY, OBSTACLE(12), EMPTY, DOOR(95), EMPTY)
		},
		.rooms[95] = {
			.name = NAME_GYM,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(94), EMPTY, ITEM(3), DOOR(93, DOOR_HIGH), EMPTY)
		}, // ceiling

		.rooms[96] = {
			.name = NAME_NURSE,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(30), EMPTY, ITEM(7), EMPTY)
		},
		.rooms[97] = {
			.name = NAME_MUSIC_ROOM,
			.music = "music.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(105), EMPTY, OBSTACLE(4), DOOR(38), EMPTY)
		},
		.rooms[98] = {
			.name = NAME_OFFICE, 0,
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ITEM(5), EMPTY, DOOR(99), EMPTY)
		},
		.rooms[99] = {
			.name = NAME_OFFICE,
			.music = "spooky.ogg",
			.music_item = 22,
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(98), EMPTY, SWITCH_CLOCK, EMPTY, DOOR(100), EMPTY)
		},
		.rooms[100] = {
			.name = NAME_OFFICE, 0,
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(99), EMPTY, DOOR(28), EMPTY)
		},

		.rooms[101] = {
			.name = NAME_CAFETERIA,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY, ITEM(6), EMPTY, DOOR(102), EMPTY)
		},
		.rooms[102] = {
			.name = NAME_KITCHEN,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(101), EMPTY, OBSTACLE(5), EMPTY, OBSTACLE(12), EMPTY, ITEM(21), EMPTY)
		},
		.rooms[103] = {
			.name = NAME_LIBRARY,
			.music = "library.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(21), EMPTY, DOOR(104), EMPTY)
		},
		.rooms[104] = {
			.name = NAME_LIBRARY,
			.music = "library.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(103), EMPTY, OBSTACLE(3), EMPTY, ITEM(4), EMPTY)
		},
		.rooms[105] = {
			.name = NAME_CHEMISTRY,
			.music = "chemistry.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ITEM(8), EMPTY, OBSTACLE(6), EMPTY, DOOR(97), EMPTY)
		},
		.rooms[106] = {
			.name = NAME_ANATOMY,
			.music = "chemistry.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(39), EMPTY, HOLE(91), EMPTY)
		},
		.rooms[107] = {
			.name = NAME_AUTO_SHOP,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ITEM(13), EMPTY, DOOR(36), EMPTY)
		},

		.rooms[108] = {
			.name = NAME_BASEMENT,
			.music = "basement.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(146), EMPTY)
		},

		.elevator[1] = {0, 33, 21, 26, 11, 91},

		.room_victory = 108
	};

	maze.rooms[14].color = colorindex(COLOR_BLACK, COLOR_MAGENTA);
	maze.rooms[21].color = colorindex(COLOR_BLACK, COLOR_RED);
	maze.rooms[24].color = colorindex(COLOR_BLACK, COLOR_GREEN);
	maze.rooms[28].color = colorindex(COLOR_WHITE, COLOR_BLUE);
	maze.rooms[30].color = colorindex(COLOR_BLACK, COLOR_WHITE);
	maze.rooms[33].color = colorindex(COLOR_BLACK, COLOR_GREEN);
	maze.rooms[36].color = colorindex(COLOR_WHITE, COLOR_BLUE);
	maze.rooms[38].color = colorindex(COLOR_BLACK, COLOR_GREEN);
	maze.rooms[39].color = colorindex(COLOR_WHITE, COLOR_BLUE);
	maze.rooms[143].color = maze.rooms[43].color = colorindex(COLOR_BLACK, COLOR_CYAN);
	maze.rooms[144].color = maze.rooms[44].color = colorindex(COLOR_BLACK, COLOR_CYAN);
	maze.rooms[145].color = maze.rooms[45].color = colorindex(COLOR_BLACK, COLOR_CYAN);
	maze.rooms[146].color = maze.rooms[46].color = colorindex(COLOR_BLACK, COLOR_CYAN);

	if (cheats)
		maze.player = (struct player){.room = 15, .location.x = 0, .items = EQUIP(1) | EQUIP(2) | EQUIP(3) | EQUIP(4) | EQUIP(5) | EQUIP(6) | EQUIP(7) | EQUIP(8) | EQUIP(9) | EQUIP(11) | EQUIP(12) | EQUIP(13) | EQUIP(21)};
	else
		maze.player = (struct player){.room = 1, .location.x = 20, .items = EQUIP(1) | EQUIP(11)};

	return &maze;
}

struct maze *maze_updated(int cheats)
{
	static struct maze maze =
	{
		.rooms[1] = {
			.name = NAME_ENTRANCE,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 39, 0, 3, 4, 5),
			LAYOUT(EMPTY, OBSTACLE(9), EMPTY, DOOR(51), EMPTY, DOOR(52), EMPTY, DOOR(53), EMPTY)
		},
		.rooms[2] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 7),
			LAYOUT(EMPTY, DOOR(54), EMPTY)
		},
		.rooms[3] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 1, 6, 0, 0),
			LAYOUT(EMPTY, OBJECT("+-+\n| |\n+-+\n\n", "Elevator in hall 11: go right, move up and right again"), EMPTY)
		}, // changed
		.rooms[4] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 1, 0, 7, 0, 0),
			LAYOUT(EMPTY, DOOR(55), EMPTY)
		},
		.rooms[5] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(1, 0, 0, 8, 0, 10),
			LAYOUT(EMPTY, DOOR(56), EMPTY, DOOR(57), EMPTY)
		},
		.rooms[6] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 3, 0, 11, 0),
			LAYOUT(EMPTY, DOOR(106), EMPTY)
		}, // changed
		.rooms[7] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(2, 0, 4, 11, 12, 13),
			LAYOUT(EMPTY, DOOR(58), EMPTY, DOOR(59), EMPTY)
		},
		.rooms[8] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 5, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(60), EMPTY)
		},
		.rooms[9] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 13, 14, 0),
			LAYOUT(EMPTY, DOOR(77), EMPTY)
		},
		.rooms[10] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(5, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(85), EMPTY)
		}, // changed
		.rooms[11] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 6, 7, 0, 0, 0),
			LAYOUT(EMPTY, ELEVATOR(1), EMPTY)
		},
		.rooms[12] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 7, 0, 16, 0, 18),
			LAYOUT(EMPTY, OBSTACLE(2), EMPTY, DOOR(62), EMPTY)
		},
		.rooms[13] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(7, 0, 9, 0, 0, 29),
			LAYOUT(EMPTY, EMPTY)
		}, // changed
		.rooms[14] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 9, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(92), EMPTY)
		},
		.rooms[15] = {
			.name = NAME_WARP_ZONE,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(34, 28, 29, 11, 14, 21),
			LAYOUT(EMPTY, DOOR(39), EMPTY)
		},

		.rooms[16] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 12, 0, 19, 0),
			LAYOUT(EMPTY, EMPTY)
		}, // changed
		.rooms[17] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 19, 0, 21),
			LAYOUT(EMPTY, DOOR(63), EMPTY, DOOR(64), EMPTY, DOOR(65), EMPTY)
		}, // changed
		.rooms[18] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(12, 0, 0, 20, 0, 22),
			LAYOUT(EMPTY, DOOR(66), EMPTY)
		},
		.rooms[19] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 16, 17, 0, 23, 24),
			LAYOUT(EMPTY, EMPTY)
		}, // changed
		.rooms[20] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 18, 0, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[21] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(17, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ELEVATOR(1), EMPTY, DOOR(103), EMPTY)
		},
		.rooms[22] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(18, 22, 0, 0, 22, 0),
			LAYOUT(EMPTY, DOOR(68), EMPTY)
		},
		.rooms[23] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 19, 0, 0, 25, 26),
			LAYOUT(EMPTY, EMPTY, DOOR(67), EMPTY, EMPTY)
		}, // changed
		.rooms[24] = {
			.name = NAME_HALL,
			.music = "spooky.ogg",
			.music_item = 22,
			.neighbors = NEIGHBORS(19, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(69), EMPTY, OBSTACLE(22), EMPTY, DOOR(70), EMPTY, DOOR(71), EMPTY, DOOR(101), EMPTY)
		}, // changed
		.rooms[25] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 23, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(72), EMPTY)
		},
		.rooms[26] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(23, 0, 27, 0, 0, 0),
			LAYOUT(EMPTY, ELEVATOR(1), EMPTY)
		},
		.rooms[27] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 28, 0, 26, 0, 0),
			LAYOUT(EMPTY, DOOR(73), EMPTY, DOOR(74), EMPTY, DOOR(75), EMPTY)
		},
		.rooms[28] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 27, 0),
			LAYOUT(EMPTY, DOOR(100), EMPTY)
		},
		.rooms[29] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(13, 0, 0, 0, 30, 31),
			LAYOUT(EMPTY, OBSTACLE(12), EMPTY)
		}, // changed
		.rooms[30] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 29, 0, 0, 37, 0),
			LAYOUT(EMPTY, DOOR(96), EMPTY)
		}, // changed
		.rooms[31] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(29, 0, 0, 0, 32, 33),
			LAYOUT(EMPTY, DOOR(76), EMPTY)
		},
		.rooms[32] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 31, 0, 0, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[33] = {
			.name = NAME_STORAGE_ROOM,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(31, 0, 34, 0, 0, 0),
			LAYOUT(EMPTY, OBSTACLE(13), EMPTY, OBSTACLE(8), EMPTY, OBSTACLE(4), SWITCH_ELEVATOR, EMPTY, ELEVATOR(1), EMPTY, DOOR(78), EMPTY)
		}, // changed
		.rooms[34] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 33, 0, 35),
			LAYOUT(EMPTY, DOOR(79), EMPTY, DOOR(80), EMPTY, DOOR(81), EMPTY)
		},
		.rooms[35] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(34, 0, 36, 0, 0, 0),
			LAYOUT(EMPTY, EMPTY)
		},
		.rooms[36] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 35, 0, 0),
			LAYOUT(EMPTY, DOOR(107), EMPTY)
		},
		.rooms[37] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 30, 0, 0, 38, 0),
			LAYOUT(EMPTY, OBSTACLE(13), EMPTY)
		},
		.rooms[38] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 37, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(97), EMPTY)
		},

		.rooms[39] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(40, 0, 41, 0, 1, 0),
			LAYOUT(EMPTY, DOOR(84, DOOR_CEILING), EMPTY, OBSTACLE(13), EMPTY)
		}, // changed
		.rooms[40] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 39),
			LAYOUT(EMPTY, DOOR(82), EMPTY)
		},
		.rooms[41] = {
			.name = NAME_HALL,
			.music = "hall.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 39, 0, 0),
			LAYOUT(EMPTY, DOOR(83), EMPTY)
		},
		.rooms[42] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 84, 0, 0, 0, 0),
			LAYOUT(EMPTY, OBSTACLE(12), EMPTY, DOOR(43), EMPTY)
		},

		.rooms[43] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 143,
			LAYOUT(EMPTY, EMPTY, DOOR(45), EMPTY, EMPTY, DOOR(42), EMPTY, DOOR(46), EMPTY)
		},
		.rooms[44] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 144,
			LAYOUT(EMPTY, DOOR(144), EMPTY, DOOR(46), EMPTY, DOOR(144), EMPTY)
		},
		.rooms[45] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 145,
			LAYOUT(EMPTY, EMPTY, DOOR(144), EMPTY, DOOR(43), EMPTY, DOOR(146), EMPTY)
		},
		.rooms[46] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 146,
			LAYOUT(EMPTY, DOOR(145), EMPTY, DOOR(43), EMPTY, DOOR(44), EMPTY, EMPTY)
		},

		.rooms[143] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 43,
			LAYOUT(EMPTY, DOOR(143), EMPTY, DOOR(143), EMPTY, EMPTY, EMPTY, DOOR(145), EMPTY)
		}, // changed
		.rooms[144] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 44,
			LAYOUT(EMPTY, DOOR(44), EMPTY, DOOR(45), EMPTY, DOOR(44), EMPTY)
		},
		.rooms[145] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 45,
			LAYOUT(EMPTY, DOOR(46), EMPTY, HINT("Danger!"), DOOR(0), EMPTY, DOOR(143), EMPTY)
		},
		.rooms[146] = {
			.name = NAME_AIR_DUCTS,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			.twin = 46,
			LAYOUT(EMPTY, DOOR(108), EMPTY, DOOR(0), HINT("Danger!"), EMPTY, DOOR(45), EMPTY)
		},

		.rooms[51] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(1), EMPTY, HINT("floor 2: rooms 39, 1, 4, 8, 13, 18, 21, 28, 27"), EMPTY)
		}, // changed
		.rooms[52] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(1), EMPTY)
		},
		.rooms[53] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(1), EMPTY)
		},
		.rooms[54] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(2), EMPTY, HINT("floor 4: rooms 2, 6, 11, 16, 19, 23, 25"), EMPTY)
		}, // changed
		.rooms[55] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(4), EMPTY)
		},
		.rooms[56] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(5), EMPTY)
		},
		.rooms[57] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(5), EMPTY)
		},
		.rooms[58] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(7), EMPTY)
		},
		.rooms[59] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(7), EMPTY)
		},
		.rooms[60] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(8), EMPTY)
		},

		.rooms[62] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(12), EMPTY)
		},
		.rooms[63] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(17), EMPTY)
		},
		.rooms[64] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(17), EMPTY)
		},
		.rooms[65] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(17), EMPTY)
		},
		.rooms[66] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(18), EMPTY)
		},

		.rooms[67] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(23), EMPTY)
		}, // new

		.rooms[68] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(22), EMPTY)
		},
		.rooms[69] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY)
		},
		.rooms[70] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY)
		},
		.rooms[71] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY)
		},
		.rooms[72] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(25), EMPTY)
		},
		.rooms[73] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(27), EMPTY)
		},
		.rooms[74] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(27), EMPTY)
		},
		.rooms[75] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(27), EMPTY)
		},
		.rooms[76] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(31), EMPTY)
		},
		.rooms[77] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(9), EMPTY)
		},
		.rooms[78] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(33), EMPTY)
		},
		.rooms[79] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(34), EMPTY)
		},
		.rooms[80] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(34), EMPTY)
		},
		.rooms[81] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(34), EMPTY)
		},
		.rooms[82] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(40), EMPTY)
		},
		.rooms[83] = {
			.name = NAME_CLASSROOM,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(41), EMPTY)
		},

		.rooms[84] = {
			.name = NAME_CRAWL_SPACE,
			.music = "crawl.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 42, 0),
			LAYOUT(EMPTY, HOLE(39), EMPTY, OBSTACLE(12), EMPTY)
		},

		.rooms[85] = {
			.name = NAME_BOILER_ROOM,
			.music = "basement.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(10), EMPTY, OBSTACLE(2), EMPTY, SWITCH_WATER, EMPTY)
		}, // new

		.rooms[91] = {
			.name = NAME_ROOFTOP,
			.music = "roof.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(106), EMPTY, OBSTACLE(13), EMPTY, ELEVATOR(1), EMPTY, OBSTACLE(12), EMPTY, ITEM(9), EMPTY)
		}, // changed
		.rooms[92] = {
			.name = NAME_LOCKERS,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(14), EMPTY, ITEM(2), EMPTY, OBSTACLE(24), EMPTY, DOOR(93), EMPTY)
		}, // changed
		.rooms[93] = {
			.name = NAME_GYM,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(HOLE(95), DOOR(92), EMPTY, DOOR(94), EMPTY)
		}, // scoreboard
		.rooms[94] = {
			.name = NAME_WEIGHT_ROOM,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(93), EMPTY, ITEM(12), EMPTY, OBSTACLE(12), EMPTY, DOOR(95), EMPTY)
		},
		.rooms[95] = {
			.name = NAME_GYM,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(94), EMPTY, ITEM(3), DOOR(93, DOOR_HIGH), EMPTY)
		}, // ceiling

		.rooms[96] = {
			.name = NAME_NURSE,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(30), EMPTY, ITEM(7), EMPTY)
		},
		.rooms[97] = {
			.name = NAME_MUSIC_ROOM,
			.music = "music.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(105), EMPTY, OBSTACLE(4), DOOR(38), EMPTY)
		},
		.rooms[98] = {
			.name = NAME_OFFICE, 0,
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ITEM(5), EMPTY, DOOR(99), EMPTY)
		},
		.rooms[99] = {
			.name = NAME_OFFICE,
			.music = "spooky.ogg",
			.music_item = 22,
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(98), EMPTY, SWITCH_CLOCK, EMPTY, DOOR(100), EMPTY)
		},
		.rooms[100] = {
			.name = NAME_OFFICE, 0,
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(99), EMPTY, DOOR(28), EMPTY)
		},

		.rooms[101] = {
			.name = NAME_CAFETERIA,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(24), EMPTY, ITEM(6), EMPTY, DOOR(102), EMPTY)
		},
		.rooms[102] = {
			.name = NAME_KITCHEN,
			.music = "gym.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(101), EMPTY, OBSTACLE(5), EMPTY, OBSTACLE(12), EMPTY, ITEM(21), EMPTY)
		},
		.rooms[103] = {
			.name = NAME_LIBRARY,
			.music = "library.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(21), EMPTY, DOOR(104), EMPTY)
		},
		.rooms[104] = {
			.name = NAME_LIBRARY,
			.music = "library.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(103), EMPTY, OBSTACLE(3), EMPTY, ITEM(4), EMPTY)
		},
		.rooms[105] = {
			.name = NAME_CHEMISTRY,
			.music = "chemistry.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ITEM(8), EMPTY, OBSTACLE(6), EMPTY, DOOR(97), EMPTY)
		},
		.rooms[106] = {
			.name = NAME_ANATOMY,
			.music = "chemistry.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(6), EMPTY, HINT("The skin, weighing about 2kg is the heaviest organ."), EMPTY, HOLE(91), EMPTY)
		}, // changed
		.rooms[107] = {
			.name = NAME_AUTO_SHOP,
			.music = "classroom.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, ITEM(13), EMPTY, DOOR(36), EMPTY)
		},

		.rooms[108] = {
			.name = NAME_BASEMENT,
			.music = "basement.ogg",
			.neighbors = NEIGHBORS(0, 0, 0, 0, 0, 0),
			LAYOUT(EMPTY, DOOR(146), EMPTY)
		},

		.elevator[1] = {0, 33, 21, 26, 11, 91},

		.room_victory = 108
	};

	maze.rooms[10].color = colorindex(COLOR_BLACK, COLOR_YELLOW);
	maze.rooms[14].color = colorindex(COLOR_BLACK, COLOR_MAGENTA);
	maze.rooms[21].color = colorindex(COLOR_BLACK, COLOR_RED);
	maze.rooms[24].color = colorindex(COLOR_BLACK, COLOR_GREEN);
	maze.rooms[28].color = colorindex(COLOR_WHITE, COLOR_BLUE);
	maze.rooms[30].color = colorindex(COLOR_BLACK, COLOR_WHITE);
	maze.rooms[33].color = colorindex(COLOR_BLACK, COLOR_GREEN);
	maze.rooms[36].color = colorindex(COLOR_WHITE, COLOR_BLUE);
	maze.rooms[38].color = colorindex(COLOR_BLACK, COLOR_GREEN);
	maze.rooms[39].color = colorindex(COLOR_WHITE, COLOR_BLUE);
	maze.rooms[143].color = maze.rooms[43].color = colorindex(COLOR_BLACK, COLOR_CYAN);
	maze.rooms[144].color = maze.rooms[44].color = colorindex(COLOR_BLACK, COLOR_CYAN);
	maze.rooms[145].color = maze.rooms[45].color = colorindex(COLOR_BLACK, COLOR_CYAN);
	maze.rooms[146].color = maze.rooms[46].color = colorindex(COLOR_BLACK, COLOR_CYAN);

	if (cheats)
		maze.player = (struct player){.room = 15, .location.x = 0, .items = EQUIP(1) | EQUIP(2) | EQUIP(3) | EQUIP(4) | EQUIP(5) | EQUIP(6) | EQUIP(7) | EQUIP(8) | EQUIP(9) | EQUIP(11) | EQUIP(12) | EQUIP(13) | EQUIP(21)};
	else
		maze.player = (struct player){.room = 1, .location.x = 20, .items = EQUIP(1) | EQUIP(11)};

	return &maze;
}
