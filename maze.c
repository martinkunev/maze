#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <ncurses.h>

#define ITEMS_Y 0
#define ITEMS_X 0

#define ROOM_Y 2
#define ROOM_X 80

#define TITLE_Y 0
#define TITLE_X 84

#define MESSAGE_Y 20
#define MESSAGE_X 84

#define HEIGHT_LIMIT 8

#define CELL_WIDTH 5

#define PLAYER_POSITION(player) (((player)->location.x + CELL_WIDTH / 2) / CELL_WIDTH)

#include "maze.h"
#include "audio.h"

unsigned colorindex(unsigned char foreground, unsigned char background)
{
	static unsigned color_numbers[256] = {0};
	static unsigned number_next = 1;
	size_t index = (foreground << 4) | background;
	if (!color_numbers[index])
	{
		init_pair(number_next, foreground, background);
		color_numbers[index] = number_next++;
	}
	return color_numbers[index];
}

static const char *name(unsigned index)
{
	const char *names[] =
	{
		[1] = "Bat",
		[2] = "Towel",
		[3] = "Deweytron",
		[4] = "Digestray",
		[5] = "Sickle",
		[6] = "Sandwich",
		[7] = "Spinal Zap",
		[8] = "Embalm Fluid",
		[9] = "Gamma Gun",

		[11] = "Sneakers",
		[12] = "Spring Shoes",
		[13] = "Suction Cups",

		[21] = "Apple",
		[22] = "Clock",
		[23] = "Elevator Switch",
		[24] = "Water Valve",
	};
	return names[index];
}

static void print_clear()
{
	size_t i;
	for(i = 0; i < 24; i += 1)
	{
		move(ROOM_Y + i, ROOM_X - 1);
		clrtoeol();
	}
}

static void print(unsigned y, unsigned x, const char *text)
{
	char *end;
	char *buffer = strdup(text), *start;

	if (!buffer)
		abort();
	start = buffer;

	for(; end = strchr(start, '\n'); y += 1)
	{
		*end = 0;
		mvprintw(y, x, start);
		*end = '\n';
		start = end + 1;
	}

	mvprintw(y, x, start);
	move(0, 0);
	refresh();

	free(buffer);
}

static void print_border(unsigned length, unsigned height, unsigned offset)
{
	const unsigned y = ROOM_Y + offset;
	const unsigned width = length * CELL_WIDTH;
	size_t i;

	// walls
	if (!offset)
		mvhline(y, ROOM_X + 1, 0, width);
	mvhline(y + 1 + height, ROOM_X + 1, 0, width);
	mvvline(y + 1, ROOM_X + 1 + width, 0, height);
	mvvline(y + 1, ROOM_X, 0, height);

	// corners
	if (!offset)
	{
		mvaddch(y, ROOM_X, ACS_ULCORNER);
		mvaddch(y, ROOM_X + 1 + width, ACS_URCORNER);
	}
	mvaddch(y + 1 + height, ROOM_X, ACS_LLCORNER);
	mvaddch(y + 1 + height, ROOM_X + 1 + width, ACS_LRCORNER);

	// interior
	for(i = 1; i <= height; i += 1)
		mvhline(y + i, ROOM_X + 1, ' ', width);
}

static void print_floor(unsigned floor, int current, unsigned offset)
{
	// floor number
	mvaddch(ROOM_Y + offset + 1, ROOM_X + 3, '0' + floor);

	mvaddch(ROOM_Y + offset, ROOM_X + 7, ACS_ULCORNER);
	mvaddch(ROOM_Y + offset, ROOM_X + 9, ACS_URCORNER);
	mvaddch(ROOM_Y + offset + 2, ROOM_X + 7, ACS_LLCORNER);
	mvaddch(ROOM_Y + offset + 2, ROOM_X + 9, ACS_LRCORNER);

	mvaddch(ROOM_Y + offset, ROOM_X + 8, ACS_HLINE);
	mvaddch(ROOM_Y + offset + 1, ROOM_X + 7, ACS_VLINE);
	mvaddch(ROOM_Y + offset + 1, ROOM_X + 9, ACS_VLINE);
	mvaddch(ROOM_Y + offset + 2, ROOM_X + 8, ACS_HLINE);

	if (current)
		mvaddch(ROOM_Y + offset + 1, ROOM_X + 8, ACS_DIAMOND);
}

static void print_twin_corners(unsigned length, unsigned offset)
{
	mvaddch(ROOM_Y + offset, ROOM_X, ACS_LTEE);
	mvaddch(ROOM_Y + offset, ROOM_X + 1 + length * CELL_WIDTH, ACS_RTEE);
}

static void print_hole(unsigned position, unsigned offset)
{
	const unsigned y = ROOM_Y + offset;
	const unsigned x = ROOM_X + 1 + position * CELL_WIDTH;

	mvaddch(y, x, ACS_RTEE);
	printw("   ");
	mvaddch(y, x + 4, ACS_LTEE);
}

static void print_door(unsigned position, unsigned height, unsigned offset, const union link *restrict info)
{
	unsigned x, y;

	if (info->room.type == DOOR_HOLE)
	{
		print_hole(position, offset);
		return;
	}

	x = ROOM_X + 1 + position * CELL_WIDTH;
	if (info->room.type == DOOR_CEILING)
	{
		y = ROOM_Y + offset;

		mvaddch(y, x, ACS_TTEE);
		mvaddch(y, x + 4, ACS_TTEE);
		mvaddch(y + 5, x, ACS_LLCORNER);
		mvaddch(y + 5, x + 4, ACS_LRCORNER);

		mvhline(y + 5, x + 1, 0, 3);
	}
	else if (info->room.type == DOOR_HIGH)
	{
		y = ROOM_Y + offset + 2;

		mvhline(y, x + 1, 0, 3);
		mvhline(y + 5, x + 1, 0, 3);

		mvaddch(y, x, ACS_ULCORNER);
		mvaddch(y, x + 4, ACS_URCORNER);
		mvaddch(y + 5, x, ACS_LLCORNER);
		mvaddch(y + 5, x + 4, ACS_LRCORNER);
	}
	else
	{
		y = ROOM_Y + offset + 1 + height - 5;

		mvhline(y, x + 1, 0, 3);

		mvaddch(y, x, ACS_ULCORNER);
		mvaddch(y, x + 4, ACS_URCORNER);
		mvaddch(y + 5, x, ACS_BTEE);
		mvaddch(y + 5, x + 4, ACS_BTEE);
	}

	mvvline(y + 1, x, 0, 4);
	mvvline(y + 1, x + 4, 0, 4);

	if (info->room.type == DOOR_ELEVATOR)
		mvaddch(ROOM_Y + offset + 1 + height - 6, ROOM_X + 1 + position * CELL_WIDTH + 2, '*');
	else if (info->room.number)
		mvprintw(y + 1, x + 1, "%3u", info->room.number);
}

static void print_player(unsigned x, unsigned height, unsigned offset)
{
	const unsigned y = ROOM_Y + offset + 1 + height;

	mvprintw(y - 4, ROOM_X + 1 + x + 1 + 1, "o");
	mvprintw(y - 3, ROOM_X + 1 + x + 1, "\\|/");
	mvprintw(y - 2, ROOM_X + 1 + x + 1 + 1, "O");
	mvprintw(y - 1, ROOM_X + 1 + x + 1, "/");
	mvprintw(y - 1, ROOM_X + 1 + x + 1 + 2, "\\");
}

static inline void print_item(unsigned position, unsigned height, unsigned offset)
{
	mvaddch(ROOM_Y + offset + 1 + height - 1, ROOM_X + 1 + position * CELL_WIDTH + 2, ACS_DIAMOND);
}

static inline void print_obstacle(unsigned position, unsigned height, unsigned offset)
{
	mvvline(ROOM_Y + offset + 1, ROOM_X + 1 + position * CELL_WIDTH + 2, ACS_CKBOARD, height);
}

static inline void print_switch(unsigned position, unsigned height, int used, unsigned offset)
{
	mvaddch(ROOM_Y + offset + 1 + height - 5, ROOM_X + 1 + position * CELL_WIDTH + 2, used ? ACS_DEGREE : ACS_CKBOARD);
}

static inline void print_hint(unsigned position, unsigned height, const char *decoration, unsigned offset)
{
	const char *start = decoration, *end;
	size_t width = 0, length;
	unsigned lines = 1;

	while (1)
	{
		end = strchr(start, '\n');
		if (!end)
			end = start + strlen(start);

		length = end - start;
		if (length > width)
			width = length;

		if (!*end)
			break;
		start = end + 1;
		lines += 1;
	}

	print(ROOM_Y + offset + 1 + height - lines, ROOM_X + 1 + position * CELL_WIDTH + (CELL_WIDTH - width) / 2, decoration);
}

static void print_neighbors(const struct player *restrict player, const struct room *restrict room)
{
	const unsigned width = room->count * CELL_WIDTH;
	const unsigned border_right = (player->location.x / CELL_WIDTH) + 1;
	const struct {int x, y; int symbol; int enabled;} neighbor[] =
	{
		[NEIGHBOR_TOP_LEFT] = {-1, 0, ACS_UARROW, player->location.x == 0},
		[NEIGHBOR_LEFT] = {-1, HEIGHT_LIMIT / 2, ACS_LARROW, player->location.x == 0},
		[NEIGHBOR_BOTTOM_LEFT] = {-1, HEIGHT_LIMIT + 1, ACS_DARROW, player->location.x == 0},
		[NEIGHBOR_TOP_RIGHT] = {width + 2, 0, ACS_UARROW, border_right == room->count},
		[NEIGHBOR_RIGHT] = {width + 2, HEIGHT_LIMIT / 2, ACS_RARROW, border_right == room->count},
		[NEIGHBOR_BOTTOM_RIGHT] = {width + 2, HEIGHT_LIMIT + 1, ACS_DARROW, border_right == room->count},
	};
	size_t i;

	for(i = 0; i < NEIGHBORS_LIMIT; i += 1)
		if (room->neighbors[i])
		{
			if (neighbor[i].enabled)
				attron(COLOR_PAIR(colorindex(COLOR_YELLOW, COLOR_BLACK)));
			mvaddch(ROOM_Y + neighbor[i].y, ROOM_X + neighbor[i].x, neighbor[i].symbol);
			attron(COLOR_PAIR(colorindex(COLOR_WHITE, COLOR_BLACK)));
		}
}

static void print_required(unsigned index)
{
	if (index == 23)
		mvprintw(MESSAGE_Y, MESSAGE_X, "Elevator is stuck");
	else
		mvprintw(MESSAGE_Y, MESSAGE_X, "requires %s", name(index));
	move(0, 0);
	refresh();
}

static void print_message(const char *message)
{
	print(MESSAGE_Y, MESSAGE_X, message);
}

static void print_room(struct maze *restrict maze, const struct room *restrict room, const struct player *restrict player, int offset)
{
	const unsigned length = room->count * CELL_WIDTH + 1;
	unsigned height = HEIGHT_LIMIT + (room->twin ? -3 : 0);
	size_t i;

	attron(COLOR_PAIR(colorindex(COLOR_WHITE, COLOR_BLACK)));

	mvprintw(MESSAGE_Y, MESSAGE_X, "\n");

	if (!offset)
		mvprintw(TITLE_Y, TITLE_X, "%s (room %i)\n", room->name, player->room);

	if (room->color)
		attron(COLOR_PAIR(room->color));

	print_border(room->count, height, offset);
	if (offset)
		print_twin_corners(room->count, offset);

	for(i = 0; i < room->count; i += 1)
	{
		switch (room->layout[i].type)
		{
		case OBJECT_DOOR:
			print_door(i, height, offset, &room->layout[i].info);
			break;

		case OBJECT_ITEM:
			if (!(player->items & EQUIP(room->layout[i].info.position)))
				print_item(i, height, offset);
			break;

		case OBJECT_OBSTACLE:
			print_obstacle(i, height, offset);
			break;

		case OBJECT_SWITCH:
			print_switch(i, height, (player->items & EQUIP(room->layout[i].info.position)), offset);
			break;

		case OBJECT_HINT:
			print_hint(i, height, room->layout[i].info.hint.decoration, offset);
			break;

		default:
			break;
		}
	}

	if (player->room == room - maze->rooms) // player is in the current room
	{
		attron(COLOR_PAIR(colorindex(COLOR_YELLOW, COLOR_BLACK)));
		print_player(player->location.x, height, offset);
	}

	attron(COLOR_PAIR(colorindex(COLOR_WHITE, COLOR_BLACK)));

	{
		unsigned position = PLAYER_POSITION(player);
		const struct object *object = &room->layout[position];
		if (((object->type == OBJECT_ITEM) || (object->type == OBJECT_SWITCH)) && !(player->items & EQUIP(object->info.position)))
			mvprintw(MESSAGE_Y, MESSAGE_X, "%s\n", name(object->info.position));
	}

	print_neighbors(player, room);

	refresh();
}

static unsigned print_elevator(const struct player *restrict player, const unsigned char *restrict elevator)
{
	int floor = -1;
	int floor_min = -1, floor_max = FLOORS_LIMIT - 1;
	int i;

	for(i = 0; i < FLOORS_LIMIT; i += 1)
	{
		if (elevator[i])
		{
			if (floor_min < 0)
				floor_min = i;

			// Check if the player is on this floor.
			if (player->room == elevator[i])
				floor = i;
		}
		else
		{
			if (floor_min >= 0)
			{
				floor_max = i - 1;
				break;
			}
		}
	}
	assert((floor >= 0) && (floor_min >= 0) && (floor_max >= floor_min));

	print_clear();
	mvprintw(TITLE_Y, TITLE_X, "Elevator\n");
	print_border(2, (floor_max - floor_min + 1) * 4 + 1, 0);
	for(i = floor_max; i >= floor_min; i -= 1)
		print_floor(i, floor == i, 2 + (floor_max - i) * 4);
	move(0, 0);
	refresh();

	return floor;
}

static void print_items(struct maze *restrict maze)
{
	size_t i;
	for(i = 1; i <= 9; i += 1)
		if (maze->player.items & EQUIP(i))
			mvprintw(ITEMS_Y + i, ITEMS_X, name(i));
	for(i = 11; i <= 19; i += 1)
		if (maze->player.items & EQUIP(i))
			mvprintw(ITEMS_Y + i, ITEMS_X, name(i));
	if (maze->player.items & EQUIP(21))
		mvprintw(ITEMS_Y + 21, ITEMS_X, name(21));
	refresh();
}

static unsigned requires(const struct room *restrict room, const struct player *restrict player, unsigned position)
{
	const struct object *object = &room->layout[position];
	if (object->type == OBJECT_OBSTACLE)
		return (player->items & EQUIP(object->info.position)) ? 0 : object->info.position;
	else
		return 0;
}

static unsigned room_position(const struct room *restrict rooms, const struct player *restrict player, unsigned from, unsigned room_new)
{
	size_t i;
	const struct object *object = rooms[room_new].layout;

	if (from)
	{
		for(i = 0; i < rooms[room_new].count; i += 1)
		{
			if ((room_new == from) && (PLAYER_POSITION(player) == i))
				continue; // ignore the door we just entered
			switch (object[i].type)
			{
			case OBJECT_DOOR:
				if (object[i].info.room.number == from)
					return i;
				break;
			}
		}
	}
	else // coming from elevator
	{
		for(i = 0; i < rooms[room_new].count; i += 1)
			if ((object[i].type == OBJECT_DOOR) && (object[i].info.room.type == DOOR_ELEVATOR))
				return i;
	}

	return 0;
}

static void input_elevator(const struct maze *restrict maze, struct player *restrict player, struct context_audio *restrict context)
{
	unsigned floor;

	const unsigned char *elevator = maze->elevator[player->elevator];

#if defined(SOUND_ON)
	audio_stop(context);
#endif

	floor = print_elevator(player, elevator);
	while (1)
	{
		switch (getch())
		{
		case KEY_UP:
			if ((floor < FLOORS_LIMIT - 1) && elevator[floor + 1])
				player->room = elevator[floor + 1];
			return;

		case KEY_DOWN:
			if (floor && elevator[floor - 1])
				player->room = elevator[floor - 1];
			return;

		case '\n':
			player->elevator = 0;
			player->location.x = room_position(maze->rooms, player, 0, player->room) * CELL_WIDTH;
			return;

		case 'q':
			endwin();
			exit(0);
		}
	}
}

static void play_music(const struct player *restrict player, struct context_audio *restrict context, const struct room *restrict room)
{
#if defined(SOUND_ON)
	if (!room->music || (player->items & EQUIP(room->music_item)))
	{
		audio_stop(context);
		return;
	}

	audio_play(context, room->music);
#endif
}

static int player_move_left(const struct room *restrict rooms, struct player *restrict player)
{
	if (player->location.x) // there is space to the left
	{
		int border_left = ((int)player->location.x - 1) / CELL_WIDTH;
		unsigned item = requires(rooms + player->room, player, border_left);
		if (item)
			print_required(item);
		else
		{
			player->location.x -= 1;
			return 1;
		}
	}
	else
	{
		unsigned neighbor;
		if (neighbor = rooms[player->room].neighbors[NEIGHBOR_LEFT])
		{
			player->room = neighbor;
			player->location.x = (rooms[neighbor].count - 1) * CELL_WIDTH;
			return 1;
		}
	}
	return 0;
}

static int player_move_right(const struct room *restrict rooms, struct player *restrict player)
{
	unsigned border_right = (player->location.x / CELL_WIDTH) + 1;
	if (border_right < rooms[player->room].count) // there is space to the right
	{
		unsigned item = requires(rooms + player->room, player, border_right);
		if (item)
			print_required(item);
		else
		{
			player->location.x += 1;
			return 1;
		}
	}
	else
	{
		unsigned neighbor;
		if (neighbor = rooms[player->room].neighbors[NEIGHBOR_RIGHT])
		{
			player->room = neighbor;
			player->location.x = 0;
			return 1;
		}
	}
	return 0;
}

static int player_move_up(const struct room *restrict rooms, struct player *restrict player)
{
	unsigned border_right = (player->location.x / CELL_WIDTH) + 1;
	unsigned neighbor;

	if (!player->location.x) // no space to the left
	{
		neighbor = rooms[player->room].neighbors[NEIGHBOR_TOP_LEFT];
		if (neighbor)
		{
			player->room = neighbor;
			player->location.x = (rooms[neighbor].count - 1) * CELL_WIDTH;
			return 1;
		}
	}
	else if (border_right == rooms[player->room].count) // no space to the right
	{
		neighbor = rooms[player->room].neighbors[NEIGHBOR_TOP_RIGHT];
		if (neighbor)
		{
			player->room = neighbor;
			player->location.x = 0;
			return 1;
		}
	}
	else if (player->location.x % CELL_WIDTH == 0)
	{
		unsigned position = PLAYER_POSITION(player);
		const struct object *object = &rooms[player->room].layout[position];
		switch (object->type)
		{
			unsigned room_new;

		case OBJECT_DOOR:
			if (object->info.room.type == DOOR_ELEVATOR)
			{
				if (player->items & EQUIP(23))
					player->elevator = object->info.room.number;
				else
				{
					print_required(23);
					return 0;
				}
			}
			else if (object->info.room.type == DOOR_HOLE)
				return 0;
			else
			{
				room_new = rooms[player->room].layout[position].info.room.number;
				player->location.x = room_position(rooms, player, player->room, room_new) * CELL_WIDTH;
				player->room = room_new;
			}
			return 1;

		case OBJECT_SWITCH:
			player->items |= EQUIP(rooms[player->room].layout[position].info.position);
			return 1;

		case OBJECT_HINT:
			print_message(rooms[player->room].layout[position].info.hint.message);
			return 0;

		default:
			return 0;
		}
	}
	return 0;
}

static int player_move_down(const struct room *restrict rooms, struct player *restrict player)
{
	unsigned border_right = (player->location.x / CELL_WIDTH) + 1;
	unsigned neighbor;

	if (!player->location.x) // no space to the left
	{
		neighbor = rooms[player->room].neighbors[NEIGHBOR_BOTTOM_LEFT];
		if (neighbor)
		{
			player->room = neighbor;
			player->location.x = (rooms[neighbor].count - 1) * CELL_WIDTH;
			return 1;
		}
	}
	else if (border_right == rooms[player->room].count) // no space to the right
	{
		neighbor = rooms[player->room].neighbors[NEIGHBOR_BOTTOM_RIGHT];
		if (neighbor)
		{
			player->room = neighbor;
			player->location.x = 0;
			return 1;
		}
	}
	else if (player->location.x % CELL_WIDTH == 0)
	{
		unsigned position = PLAYER_POSITION(player);
		unsigned item;

		if (rooms[player->room].layout[position].type != OBJECT_ITEM)
			return 0;

		item = rooms[player->room].layout[position].info.position;
		player->items |= EQUIP(item);
	}
	return 1;
}

static void play(struct maze *restrict maze, struct context_audio *restrict context)
{
	char in;

	const struct room *rooms = maze->rooms;
	struct player *player = &maze->player;

	play_music(player, context, &rooms[player->room]);
	print_items(maze);
	print_clear();

	if (rooms[player->room].twin)
	{
		const struct room *first, *second;
		if (rooms[player->room].twin > player->room)
		{
			first = rooms + rooms[player->room].twin;
			second = rooms + player->room;
		}
		else
		{
			first = rooms + player->room;
			second = rooms + rooms[player->room].twin;
		}
		print_room(maze, first, player, 0);
		print_room(maze, second, player, 6);
	}
	else
		print_room(maze, rooms + player->room, player, 0);

	move(0, 0);
	while (1)
	{
		switch (getch())
		{
			unsigned neighbor;

		case KEY_LEFT:
			if (player_move_left(rooms, player))
				return;
			break;

		case KEY_RIGHT:
			if (player_move_right(rooms, player))
				return;
			break;

		case KEY_UP:
		case '\n':
			if (player_move_up(rooms, player))
				return;
			break;

		case KEY_DOWN:
			if (player_move_down(rooms, player))
				return;
			break;

		case 'q':
			endwin();
			exit(0);
		}
	}
}

int main(int argc, char *argv[])
{
	unsigned index;
	const char *message = "Victory!";
	struct maze *maze;

	struct context_audio context;

	if (argc != 2)
	{
		fprintf(stderr, "usage: %s <maze>\n\t0 - original; no cheats\n\t1 - original; cheats\n\t2 - updated; no cheats\n\t3 - updated; cheats\n", argv[0]);
		return 1;
	}
	index = strtol(argv[1], 0, 10);
	if (index >= 4)
	{
		fprintf(stderr, "invalid maze. use one of: 0 1 2 3\n");
		return 2;
	}

#if defined(SOUND_ON)
	if (audio_init(&context) < 0)
		abort();
#endif

	initscr();
	raw();
	noecho();
	keypad(stdscr, TRUE);
	start_color();

	switch (index)
	{
	case 0:
		maze = maze0(0);
		break;

	case 1:
		maze = maze0(1);
		break;

	case 2:
		maze = maze_updated(0);
		break;

	case 3:
		maze = maze_updated(1);
		break;
	}

	init_color(COLOR_WHITE, 1000, 1000, 1000);
	init_color(COLOR_MAGENTA, 860, 160, 300);

	print_items(maze);
	while (maze->player.room != maze->room_victory)
	{
		if (maze->player.elevator)
			input_elevator(maze, &maze->player, &context);
		else
			play(maze, &context);

		if (!maze->player.room)
		{
			message = "You entered into a trap! Game over.";
			break;
		}
	}

	attron(A_BOLD);
	mvprintw(MESSAGE_Y, MESSAGE_X, message);
	attroff(A_BOLD);
	refresh();
	sleep(1);
	getch();

	endwin();

#if defined(SOUND_ON)
	audio_term(&context);
#endif

	return 0;
}
